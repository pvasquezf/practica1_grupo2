function multiplicar_Potencia(numero1, numero2, potencia = false) {
    if (typeof numero1 === 'number') {
        if (typeof numero2 === 'number') {
            if (potencia) {
                return Math.pow(numero1, numero2)
            } else {
                return numero1 * numero2;
            }
        } else {
            return "El numero 2 no es valido.";
        }
    } else {
        return "El numero 1 no es valido.";
    }
}
console.log(multiplicar_Potencia(2, 3, true)); // 8
console.log(multiplicar_Potencia(2, 3, false)); // 6
console.log(multiplicar_Potencia(2, 3)); // 6

