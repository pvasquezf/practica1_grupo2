function potenciaCubica(numero, base) {
	return "El numero elevado a la "+ base+" es: "+ Math.pow(numero, base);
}
console.log(potenciaCubica(3,4)); 

function raizCubica(numero) {	

	if(isNaN(numero))
	{
		return "No es un numero por lo que no se puede obtener raiz cubica";
	}
	else
	{
		var raizc = Math.cbrt(numero);
		if(Number.isInteger(raizc))
		{
			return "La raiz cubica exacta del numero "+numero+" es: " + raizc + ", es un cubo perfecto";
		}
		else
		{
			return "El numero "+numero+" no tiene raiz cubica exacta, su aproximacion es: " + raizc + ", no es un cubo perfecto";
		}
	}
    
}
console.log(raizCubica(64)); 