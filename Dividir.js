function dividir(numero1, numero2) {
    if(numero2 == 0){
        return "El divisor no puede ser cero.";
    }
    return numero1 / numero2;
}
console.log(dividir(28, 0));